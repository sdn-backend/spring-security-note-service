package com.springsecurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpriingSecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpriingSecurityApplication.class, args);
	}

}
