# Spring Boot + Spring Security + JPA 개발
> Spring Security + Thymeleaf + Lombok + JPA + h2

## 목차
1. [구현 방법](#1_구현_방법)
2. [프로젝트 구조](#2_프로젝트_구조)

## 1 구현 방법
### 1. 초기 세팅
- Spring Web
- Spring Security
- Thymeleaf
- Lombok
- Spring JPA
- H2
- Gradle

### 2. 뷰단 구현
- `thymeleaf 시큐리티 모듈 가져오기`
```java
xmlns:sec="http://www.thymeleaf.org/extras/spring-security"
```
- 권한별 메뉴바 상태

|인증|코드|
|---|---|
|인증 받은 모든 사람|sec:authorize="isAuthenticated()"|
|인증 받지 못한 사람|sec:authorize="!isAuthenticated()"|
|관리자|sec:authorize="hasAnyRole("ROLE ADMIN")|
|사용자|sec:authorize="hasAnyRole("ROLE USER")|

### 3. 서비스단 개발
1. `user` `notice` `note` `admin` 개발
2. `config` 개발
    - `SpringSecurityConfig` ⭐
        - 페이지별 권한 설정
        - 로그인 
        - 로그아웃  
        - `userDetilsService` 정보 가져오기
    - `InitializeDefault`
        - 기본 유저, 어드민 정보 저장
    - `MvcConfig`
        - /home, /, /login url별 뷰페이지 기본 설정
   
### 4. 테스트
> BDDAssertions, MockMvc
1. 각 도메인 테스트 진행
2. 결과 

![](img/test.jpg)

## 2 프로젝트 구조
